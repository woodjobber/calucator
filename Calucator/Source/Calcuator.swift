//
//  Calcuator.swift
//  Calucator
//
//  Created by chengbin on 2021/4/8.
//

import UIKit

class Calcuator: NSObject {
    var result: Double? {
        didSet {
            if result != nil {
                operand = result
            }
        }
    }
    /// 操作数
    private var operand: Double?
    /// 记录二元操作
    private var bOperation: BinaryOperation?
    
    func saveOperand(_ operand: Double) {
        result = operand
        self.operand = result
    }
    /// 执行结算
    func performResult(_ symbol: String) {
        let operation = operations[symbol]!
        
        switch operation {
        case let .zero(value):
            result = value
            bOperation = nil
        case .equal:
            guard let b = bOperation, let o = operand else {
                return
            }
            result = b.perform(secound: o)
            
        case let .unary(function):
            guard let o = operand else {
                return
            }
            result = function(o)

        case let .binary(function):
            guard let o = operand else {
                return
            }
            if bOperation != nil {
                _ = bOperation?.perform(secound: 0)
                /// 这里还需要处理.. 需要考虑 连续输入的情况.
            }
            bOperation = BinaryOperation(lsh: o, function: function)
            result = nil
        case .dot:
            break
        case .else:
            break
        }
    }
    
    private struct  BinaryOperation {
        let lsh: Double
        let function: (Double, Double) -> Double
        func perform(secound rsh: Double) -> Double {
            return function(lsh, rsh)
        }
    }
    /// 在这里可以扩展操作符，以方便后续
    private enum Operation {
        case equal
        case zero(Double)
        case unary((Double) -> Double)
        case binary((Double, Double) -> Double)
        case dot
        /// 其它操作符
        case `else`
    }
    
    private var operations: [String: Operation] = [
        "+": .binary(+),
        "−": .binary(-),
        "×": .binary(*),
        "/": .binary(/),
        "AC": .zero(0),
//        "+/-": .unary { -$0 },
        "%": .unary { $0 / 100 },
        "=": .equal,
        ".": .dot,
        "+/-":.unary({ (a) -> Double in
            -a
        })
    ]
}
