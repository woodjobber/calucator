//
//  CalculatorViewController.swift
//  Calucator
//
//  Created by chengbin on 2021/4/8.
//

import UIKit
// 题目：
//实现一个界面，含有一个“输入框”和一个“按钮”。 在输入框里，会输入两个自然数的运算，例如：1+2、3*4、6/5 … 点击按钮，将输入框里面的算式的结果输出到输入框中。
//举例说明： 输入框中输入：100/10，点击按钮，输入框中显示：10 要求：1. 请用面向对象的思想实现以上需求。 2. 你的程序只需要处理加减乘除这四种运算符，但需要注意，你的程序必须考虑 未来支持其他运算符的可能性，尽量小的改动来支持运算符扩展。 3. 请完成后将项目以姓名命名，微信发送至 HR。


// 此题目可以看做就是一个计算器，为了方便不处理键盘问题，现讲数字以及可操作的符号 显示到界面，没有UI，参照苹果计算器。运用组件化思想，提供通用能力。
// 为了快速布局，使用 sb 方式

class CalculatorViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private var displayResult: Double {
        get {
            return Double(resultLabel.text!)!
        }
        set {
            if newValue.isInfinite {
                resultLabel.text = "NaN"
                return
            }
            if Double(Int(newValue)) == newValue {
                resultLabel.text = String(Int(newValue))
            } else {
                resultLabel.text = String(newValue)
            }
        }
    }

    private var calcuator = Calcuator()

    private var isTyping: Bool = false

    @IBAction func performDigital(_ sender: UIButton) {
        let operand = sender.currentTitle!
        let text = resultLabel.text!
        guard isTyping else {
            isTyping = true
            if operand == "." {
                resultLabel.text = "0."
            } else {
                resultLabel.text = operand
            }
            return
        }
        
        if operand == "0", text == "0" {
            resultLabel.text = "0"
        } else {
            resultLabel.text = text + operand
        }
    }

    @IBAction func performOperator(_ sender: UIButton) {
        if isTyping {
            calcuator.saveOperand(displayResult)
            isTyping = false
        }
        let operand = sender.currentTitle!
        calcuator.performResult(operand)
        if let result = calcuator.result {
            displayResult = result
        }
    }
}

extension CalculatorViewController {
    public static func instance() -> CalculatorViewController {
        return UIStoryboard(name: "Calculator", bundle: nil).instantiateViewController(identifier: "CalculatorViewController")
    }

    public static func show() {
        let calculatorViewController = CalculatorViewController.instance()
        calculatorViewController.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        UIApplication.shared.windows.first?.rootViewController?.present(calculatorViewController, animated: true) {}
    }
}
