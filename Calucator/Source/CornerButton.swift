//
//  CornerButton.swift
//  Calucator
//
//  Created by chengbin on 2021/4/8.
//

import UIKit

class CornerButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.size.height * 0.5
        layer.masksToBounds = true
    }
}
