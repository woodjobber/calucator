//
//  ViewController.swift
//  Calucator
//
//  Created by chengbin on 2021/4/8.
//

import UIKit
private enum Operation {
    case equal
    case constant(Double)
    case unary((Double) -> Double)
    case binary((Double, Double) -> Double)
}
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let top = Operation.binary(+)
        
        switch top {
        case let .binary(f):
            let r  = f(2,2)
            print(r)
        default:
            break
        }
    }

    @IBAction func showCalculator(_ sender: UIButton) {
        CalculatorViewController.show()
    }
}
